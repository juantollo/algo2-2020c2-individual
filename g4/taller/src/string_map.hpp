#include <vector>
#include <string>
#include "string_map.h"

template<typename T>
string_map<T>::string_map():_raiz(NULL), _size(0) {}

template<typename T>
string_map<T>::string_map(const string_map<T> &aCopiar)
        : string_map() { *this = aCopiar; } // Provisto por la catedra: utiliza el operador asignacion para realizar la copia.


template<typename T>
void string_map<T>::insert(const pair<string, T> &t) {
    if (this->empty()) {
        _raiz = new Nodo();
    }
    string clave = t.first;
    //Nodo que va ir recorriendo la estructura
    Nodo *nodoActual = _raiz;

    // Recorro el string para ir insertando nodos
    for (int i = 0; i < clave.length(); i++) {
        //(i = clave.length() - 1) {
        //  nodoActual->siguientes[int(clave[i])] = nodoDef;

        // Chequeo si la letra está definida y sino está la defino...
        if (nodoActual->siguientes[int(clave[i])] == nullptr) {
            Nodo *intermedio = new Nodo();
            nodoActual->siguientes[int(clave[i])] = intermedio;
            nodoActual = intermedio;
            // La letra está definida entonces pongo mi nodo actual en esa letra
        } else {
            nodoActual = nodoActual->siguientes[int(clave[i])];
        }

    }
    if (nodoActual->definicion == nullptr) {
        this->_size++;
    } else { delete (nodoActual->definicion); }
    nodoActual->definicion = new T(t.second);
}


template<typename T>
void string_map<T>::copiarNodos(Nodo* &a, Nodo *b) {
    if (b == nullptr) {
        a = nullptr;
        return;
    } else
        {a = new Nodo();
        }

    if (b->definicion != nullptr) {
        a->definicion = new T(*b->definicion);
    }

    for (int i = 0; i < b->siguientes.size(); i++) {
        copiarNodos(a->siguientes[i], b->siguientes[i]);
    }
}


template<typename T>
string_map<T> &string_map<T>::operator=(const string_map<T> &d) {
    copiarNodos(_raiz, d._raiz);
    _size = d.size();
    return *this;
}


template<typename T>
void string_map<T>::destruir(Nodo* n) {
    if (n != nullptr) {

        for (int i = 0; i < 256; i++) {
            if (n->siguientes[i] != nullptr) {
                destruir(n->siguientes[i]);
            }
        }
        if (n->definicion != nullptr) {
            delete n->definicion;
        }
        delete n;
    }
}
template<typename T>

string_map<T>::~string_map() {
    destruir(_raiz);
}

template<typename T>
T &string_map<T>::operator[](const string &clave) {
    // COMPLETAR
}


template<typename T>
int string_map<T>::count(const string &clave) const {
    if (this->empty()) {
        return 0;
    } else {
        Nodo *nodoActual = _raiz;
        for (int i = 0; i < clave.length(); i++) {
            if(nodoActual != nullptr){
                nodoActual = nodoActual->siguientes[int(clave[i])];
            } else{return 0;}
        }
        if (nodoActual->definicion != nullptr) {
            return 1;
        } else {
            return 0;
        }
    }
}

template<typename T>
const T &string_map<T>::at(const string &clave) const {
    Nodo *actualNodo = _raiz;

    for (int i = 0; i < clave.length(); i++) {
        actualNodo = actualNodo->siguientes[int(clave[i])];

    }
    return *(actualNodo->definicion);
}

template<typename T>
T &string_map<T>::at(const string &clave) {
    // Me dan una clave definida y tengo que recorrerar
    // Cuando llego al utlimo nodo le pido su significado

    Nodo *actualNodo = _raiz;

    for (int i = 0; i < clave.length(); i++) {
        actualNodo = actualNodo->siguientes[int(clave[i])];

    }
    return *(actualNodo->definicion);
}

template<typename T>
typename string_map<T>::Nodo *string_map<T>::eliminar(string_map::Nodo* x, string clave, int d) {
    if (x == nullptr) {
        return nullptr;
    }
    else if (d == clave.length()) {
        T* Eliminar = x->definicion;
        x->definicion = nullptr;
        delete Eliminar;
    }
    else {
        char c = clave[d];
        x->siguientes[c] = eliminar(x->siguientes[c], clave, d + 1);
    }
    if (x->definicion != nullptr) {
        return x;
    }
    for (int i = 0; i < 256; i++) {
        if (x->siguientes[i] != nullptr) {
            return x;
        }
    }
    return x;
}

template<typename T>
void string_map<T>::erase(const string &clave) {
    _raiz = eliminar(_raiz, clave, 0);
}

template<typename T>
int string_map<T>::size() const {
    return _size;
}

template<typename T>
bool string_map<T>::empty() const {
    bool res = false;
    if (this->size() == 0) {
        res = true;
    }
    return res;
}









