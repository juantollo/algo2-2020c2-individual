#include "Lista.h"

Lista::Lista() : _primero(NULL), _ultimo(NULL), _longitud(0) {}

Lista::Lista(const Lista &l) : Lista() {
    *this = l;
}
//Inicializa una lista vacía y luego utiliza operator= para no duplicar el código de la copia de una lista.

Lista::Nodo::Nodo(int const &elem, Nodo *ant, Nodo *sig) : valor(elem), anterior(ant), siguiente(sig) {}

Lista::~Lista() {
    Nodo *temp = _primero;

    while (temp != NULL) {
        temp = temp->siguiente;
        delete (_primero);
        _primero = temp;
    }
}

Lista &Lista::operator=(const Lista &aCopiar) {
    Nodo *nodoActual = this->_primero;
    while (nodoActual != NULL) {
        nodoActual = nodoActual->siguiente;
        delete (_primero);
        this->_primero = nodoActual;
    }
    _longitud = 0;

    for (int i = 0; i < aCopiar.longitud(); i++) {
        agregarAtras(aCopiar.iesimo(i));
    }
    return *this;
}

void Lista::agregarAdelante(const int &elem) {
    Nodo* nodo = new Nodo(elem, NULL, _primero);
    if (_primero == NULL){
        _ultimo = nodo;
    }

    if (_primero != NULL) {
        _primero->anterior = nodo;
    }
    _primero = nodo;
    _longitud++;
}

void Lista::agregarAtras(const int &elem) {
    Nodo* nodo = new Nodo(elem, _ultimo, NULL);
    if (_primero != NULL) {
        _ultimo->siguiente = nodo;
    } else {
        _primero = nodo;
    }
    _ultimo = nodo;
    _longitud++;
}

void Lista::eliminar(Nat i) {

    if (longitud() == 1) {
        delete (_primero);
        // El puntero apunta a una posicion de memoria invalida...y entonces hay que eliminar las direcciones que tenian asignadas esas variables.
        _primero = NULL;
        _ultimo = NULL;
    }

    if (longitud() > 1 && i == 0) {
        Nodo *aBorrar = _primero;
        _primero = _primero->siguiente;
        delete (aBorrar);
    }

    if (longitud() > 1 && longitud() == i+1) {
        Nodo *aBorrar = _ultimo;
        _ultimo = _ultimo->anterior;
        delete (aBorrar);
    }

    else if (longitud() > 1 && i > 0) {
        Nodo *aBorrar = _primero;
        int p = 0;

        while (p < i) {
            aBorrar = aBorrar->siguiente;
            p++;
        }
        (aBorrar->anterior)->siguiente = aBorrar->siguiente;
        (aBorrar->siguiente)->anterior = aBorrar->anterior;
        delete aBorrar;
    }
    _longitud--;
}

int Lista::longitud() const {
    return _longitud;
}

const int &Lista::iesimo(Nat i) const {
    int p = 0;
    Nodo* actual = _primero;
    while (p < i) {
        actual = actual->siguiente;
        p++;
    }
    return actual->valor;
}

int &Lista::iesimo(Nat i) {
    int p = 0;
    Nodo *actual = _primero;
    while (p < i) {
        actual = actual->siguiente;
        p++;
    }
    return actual->valor;
}
void Lista::mostrar(ostream &o) {
}