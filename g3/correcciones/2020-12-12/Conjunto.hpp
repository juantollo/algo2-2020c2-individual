#include "Conjunto.h"


class Nodo;

template<class T>
Conjunto<T>::Nodo::Nodo(T const &elem, Nodo *izquierda, Nodo *derecha, Nodo *padre) : valor(elem), izq(izquierda),
                                                                                      der(derecha), p(padre) {}

template<class T>
Conjunto<T>::Conjunto(): raiz(NULL), _cardinal(0) {
}
// El constructor define la raiz vacía.

template<class T>
unsigned int Conjunto<T>::cardinal() const {
    return _cardinal;
}


template<class T>
void Conjunto<T>::destruir(Nodo *n) {
    if (n != NULL) {
        destruir(n->izq);
        destruir(n->der);
        delete n;
    }
}

template<class T>
Conjunto<T>::~Conjunto() {
    destruir(raiz);
}

template<class T>
bool Conjunto<T>::pertenece(const T &clave) const {
    bool result = false;
    Nodo *y;
    Nodo *x = raiz;
    if (raiz == NULL) {
        return result;
    }
    if (raiz->valor == clave) {
        result = true;
        return result;
    } else {
        while (x != NULL) {
            if (x->valor == clave) {
                result = true;
                return result;
            } else {
                y = x;
                if (clave < x->valor) {
                    x = x->izq;
                } else {
                    x = x->der;
                }
            }

        }

    }
    return result;
}

template<class T>
void Conjunto<T>::insertar(const T &clave) {

    Nodo *y = NULL;
    Nodo *x = raiz;
    Nodo *z = new Nodo(clave, NULL, NULL, NULL);

    while (x != NULL) {
        y = x;
        if (z->valor < x->valor) {
            x = x->izq;
        } else if(z->valor == x->valor){
            return;
        }
        else {
            x = x->der;
        }
    }
    z->p = y;
    if (y == NULL) {
        raiz = z;
    } else if (z->valor < y->valor) {
        y->izq = z;
    } else {
        y->der = z;
    }
    _cardinal++;
}

template<class T>
void Conjunto<T>::transplantar(Nodo *u, Nodo *v) {
    if (u->p == NULL) {
        raiz = v;
    } else if (u == u->p->izq) {
        u->p->izq = v;
    } else {
        u->p->der = v;
    }
    if (v != NULL) {
        v->p = u->p;
    }
}

template<class T>
void Conjunto<T>::remover(const T &clave) {

    Nodo *z;
    z = buscar_nodo(clave);
    if (z->izq == NULL) {
        transplantar(z, z->der);
    } else if (z->der == NULL) {
        transplantar(z, z->izq);
    } else {
        Nodo *y = buscar_nodo(minimo(z->der));
        if (y->p != z) {
            transplantar(y, y->der);
            y->der = z->der;
            y->der->p = y;
        }
        transplantar(z, y);
        y->izq = z->izq;
        y->izq->p = y;
    }
    _cardinal = _cardinal - 1;
}


template<class T>
const T &Conjunto<T>::siguiente(const T &clave) {
    Nodo *x = buscar_nodo(clave);

    // x es nodo con `x->valor == clave`

    if (x->der != nullptr) {
        return minimo(x->der);
    }

    Nodo *y = x->p;
    while (y != nullptr && x == y->der) {
        x = y;
        x = y->p;
    }

    return y->valor;
}

template<class T>
const T &Conjunto<T>::minimo() const {
    return minimo(raiz);
}

template<class T>
const T &Conjunto<T>::minimo(Nodo *x) const {
    while (x->izq != NULL) {
        x = x->izq;
    }
    return x->valor;
}

template<class T>
const T &Conjunto<T>::maximo() const {
    Nodo *x = raiz;
    while (x->der != NULL) {
        x = x->der;
    }
    return x->valor;
}

template<class T>
void Conjunto<T>::mostrar(std::ostream &) const {
    assert(false);
}

template<class T>
typename Conjunto<T>::Nodo *Conjunto<T>::buscar_nodo(const T &clave) const {
    Nodo *x = raiz;
    while (x != NULL && x->valor != clave) {
        if (clave < x->valor) {
            x = x->izq;
        } else {
            x = x->der;
        }
    }

    return x;
}
