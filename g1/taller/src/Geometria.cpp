#include <iostream>

using namespace std;

using uint = unsigned int;

// Ejercicio 1

class Rectangulo {
public:
    Rectangulo(uint alto, uint ancho);

    uint alto();

    uint ancho();

    float area();

private:
    int alto_;
    int ancho_;
};

Rectangulo::Rectangulo(uint alto, uint ancho) {
    this->alto_ = alto;
    this->ancho_ = ancho;
}

uint Rectangulo::alto() {
    return alto_;
}

uint Rectangulo::ancho() {
    return this->ancho_;
}

float Rectangulo::area() {
    float area = (alto_ * ancho_);
    return area;
}

// Ejercicio 2
const float PI = 3.14;

class Elipse {
public:
    Elipse(uint a, uint b);

    uint r_a();

    uint r_b();

    float area();

private:
    int r_b_;
    int r_a_;

};

Elipse::Elipse(uint a, uint b) {
    this->r_a_ = a;
    this->r_b_ = b;
}

uint Elipse::r_a() {
    return r_a_;
}

uint Elipse::r_b() {
    return r_b_;
}

float Elipse::area() {
    float area = PI * r_a_ * r_b_;
    return area;
}

// Ejercicio 3

class Cuadrado {
public:
    Cuadrado(uint lado);

    uint lado();

    float area();

private:
    Rectangulo r_;
};

Cuadrado::Cuadrado(uint lado) : r_(lado, lado) {}

uint Cuadrado::lado() {
    return r_.alto();
}

float Cuadrado::area() {
    float area = float(lado() * lado());
    return area;
}

// Ejercicio 4

class Circulo {
public:
    Circulo(uint radio);

    uint radio();

    float area();

private:
    Elipse e_;
};

Circulo::Circulo(uint radio) : e_(radio, radio) {}

uint Circulo::radio() {
    return e_.r_a();
}

float Circulo::area() {
    return e_.area();
}

// Ejercicio 5

ostream &operator<<(ostream &os, Elipse e) {
    os << "Elipse(" << e.r_a() << "," << e.r_b() << ")";
    return os;
}

ostream &operator<<(ostream &os, Rectangulo r) {
    os << "Rect(" << r.alto() << ", " << r.ancho() << ")";
    return os;
}

// Ejercicio 6

ostream &operator<<(ostream &os, Circulo c) {
    os << "Circ(" << c.radio() << ")";
    return os;
}

ostream &operator<<(ostream &os, Cuadrado c) {
    os << "Cuad(" << c.lado() << ")";
    return os;
}