#include <iostream>
#include <vector>

using namespace std;

using uint = unsigned int;

// Pre: 0 <= mes < 12
uint dias_en_mes(uint mes) {
    uint dias[] = {
            // ene, feb, mar, abr, may, jun
            31, 28, 31, 30, 31, 30,
            // jul, ago, sep, oct, nov, dic
            31, 31, 30, 31, 30, 31
    };
    return dias[mes - 1];
}

// Ejercicio 7, 8, 9 y 10

// Clase Fecha
class Fecha {
public:
    Fecha(int mes, int dia);

    int mes() const;

    int dia() const;

    bool operator==(Fecha o) const;

    void incrementar_dia();

private:
    int mes_;
    int dia_;
};

Fecha::Fecha(int mes, int dia) {
    mes_ = mes;
    dia_ = dia;
}

int Fecha::dia() const {
    return dia_;
}

int Fecha::mes() const {
    return mes_;
}


ostream &operator<<(ostream &os, Fecha f) {
    os << f.dia() << "/" << f.mes();
    return os;
}


bool Fecha::operator==(Fecha o) const {
    bool igual_dia = (dia() == o.dia() && mes() == o.mes());
    return igual_dia;
}


void Fecha::incrementar_dia() {
    if (dia() < dias_en_mes(mes())){
        dia_++;
    } else {
        dia_= 1;
        mes_++;
    }
}

// Ejercicio 11, 12

// Clase Horario
class Horario{
public:
    Horario(uint hora, uint min);
    uint hora() const;
    uint min() const;
    bool operator==(Horario h) const;
    bool operator<(Horario h) const;

private:
    uint hora_;
    uint min_;
};

Horario::Horario(uint hora, uint min){
    hora_ = hora;
    min_ = min;
}

uint Horario::hora() const {
    return hora_;
}

uint Horario::min() const {
    return min_;
}

ostream& operator<<(ostream& os,Horario h){
    os << h.hora() <<":"<<h.min();
    return os;
}

bool Horario::operator==(Horario h) const {
    return (this->hora() == h.hora());
}

bool Horario::operator<(Horario h) const {
    if (hora() == h.hora()) {
        return (this->min() < h.min());
    } else {
        return (this->hora() < h.hora());
    }
}
    // Ejercicio 13
// Clase Recordatorio

class Recordatorio {
public:
    Recordatorio(Fecha f,Horario h, string mensaje);
    string mensaje();
    Fecha fecha();
    Horario horario() const;
    bool operator<(Recordatorio r) const;

private:
    string mensaje_;
    Fecha f_;
    Horario h_;
 };

Recordatorio::Recordatorio(Fecha f, Horario h, string mensaje) : f_(f), h_(h), mensaje_(mensaje){
    }

 string Recordatorio::mensaje(){
     return mensaje_;
}

Fecha Recordatorio::fecha() {
    return f_;
}
Horario Recordatorio::horario() const {
    return h_;
}

bool Recordatorio::operator<(Recordatorio r) const {
    return (this->horario() < r.horario());
}

ostream& operator<<(ostream& os, Recordatorio r){
    os << r.mensaje()<<" @ "<<r.fecha()<<" "<<r.horario();
    return os;}

// Ejercicio 14

class Agenda {
public:
    Agenda(Fecha fecha_inicial);
    void agregar_recordatorio(Recordatorio rec);
    void incrementar_dia();
    vector<Recordatorio> recordatorios_de_hoy();
    Fecha hoy();

private:
    Fecha hoy_;
    vector<Recordatorio> recordatorios_;
};

Agenda::Agenda(Fecha fecha_inicial): hoy_(fecha_inicial) {
}

void Agenda::agregar_recordatorio(Recordatorio rec) {
    recordatorios_.push_back(rec);
    sort(recordatorios_.begin(), recordatorios_.end());
}

void Agenda::incrementar_dia() {
    hoy_.incrementar_dia();
}

Fecha Agenda::hoy() {
    return hoy_;
}

vector<Recordatorio> Agenda::recordatorios_de_hoy(){
    vector<Recordatorio> recordatoriosDeHoy;
    for(Recordatorio r: recordatorios_){
        if (hoy() == r.fecha()){
            recordatoriosDeHoy.push_back(r);
        }
    }
    return recordatoriosDeHoy;
}

ostream& operator<<(ostream& os, Agenda a){
    os << a.hoy() << endl;
    cout << "====="<< endl;
    for (Recordatorio r: a.recordatorios_de_hoy()){
        os << r << endl;
    }
return os;
}
